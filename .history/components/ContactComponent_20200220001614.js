import React, { Component } from "react";
import { ScrollView, Text } from "react-native";
import { Card } from "react-native-elements";

class Contact extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    //   const contactInfo = 'Contact Information';
      return(
          <ScrollView>
              <Card
                featuredTitle='Contact Information'
              >
                  <Text style={{ margin: 10 }}>
                    121, Clear Water Bay Road
                  </Text>
                  <Text style={{ margin: 10 }}>
                    Clear Water Bay, Kowlon
                  </Text>
              </Card>
          </ScrollView>
      )
  }
}

export default Contact;
