import React, { Component } from "react";
import { ScrollView, Text } from "react-native";
import { Card } from "react-native-elements";

class Contact extends Component {
  constructor(props) {
    super(props);
  }

  render() {
      const contactInfo = 'Contact Information';
      return(
          <ScrollView>
              <Card
                featuredTitle={contactInfo}
              >
                  <Text style={{ margin: 10 }}>
                      <p>121, Clear Water Bay Road</p>
                      <p>Clear Water Bay, Kowloon</p>
                      <p style={{textTransform: "capitalize"}}>Hongkong</p>
                      <p>Tel: +852 1234 5678</p>
                      <p>Fax: +852 8765 4321</p>
                      <p>Email:confusion@food.net</p>
                  </Text>
              </Card>
          </ScrollView>
      )
  }
}

export default Contact;
